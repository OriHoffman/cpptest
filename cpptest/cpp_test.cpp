#include "functions.h"
#include <iterator>
#include <map>

using namespace std;
using Satoshi = uint64_t;
using Address = uint64_t;

//The amount of money created every block and given to the block's miner
#define MONEY_CREATED_FOR_THE_MINER_EACH_BLOCK 1000
#define MAX_ENTRIES_IN_BLOCK 10

#define SIG_LENGTH 32

struct Transaction
{
    //creator of transaction and sender of funds
    Address from;

    //transaction destination
    Address to;

    //amount of money to send to the destination
    Satoshi amount;

    //amount of fee is offered by the creator to the miner to add this transaction to the blockchain
    Satoshi fee;

    //creation time, written by the transaction creator
    time_t creationTime;

    //signing {from,amount,to,fee,creation time}
    uint8_t signature[SIG_LENGTH];
    
};

struct Block
{
    //holds up to 10 transactions
    std::vector<Transaction> trans;

    //the miner that added the block to the blockchain
    Address miner;

    // the next block at the blockchain (is nullptr for the last block)
    Block* next;
public:
    bool TxExists(const Transaction tx) const
    {
        for (const Transaction &oldTx : trans)
            if (std::equal(std::begin(oldTx.signature),
                           std::end(oldTx.signature), 
                           std::begin(tx.signature))
                ) { return true; }
       return false;
    }

};
bool TxIsSane(const Transaction& tx)
{
    if (tx.from == tx.to || tx.amount == 0)
    {
        return false;
    }
    return true;
}

class Blockchain
{
    //not null
    Block* m_firstBlock;
    std::map<Address, Satoshi>* wallets = new std::map<Address, Satoshi>();
    uint32_t* lastBlockUpdate = new uint32_t(0);

public:
    //assumption firstBlock is not null
    Blockchain(Block* firstBlock) :m_firstBlock(firstBlock) {}

    //Assumption: all the transaction in the blockchain ("firstBlock linked-list") are valid
    //return whether or not the new transaction is valid, given this blockchain
    //No need to validate the crypto signature
    bool isValid(const Transaction& newTransaction)const
    {
        if (TxExists(newTransaction) || !TxIsSane(newTransaction) || !ValidFunds(newTransaction)) //make sure transaction isn't already in blockchain
        { 
            return false;
        }
            
    }

    bool ValidFunds(const Transaction& newTx)const
    {
        if (wallets->at(newTx.from) < newTx.amount + newTx.fee)
        {
            return false;
        }
        return true;
    }
    //called before isValid
    void UpdateWalltes()
    {
        Block current_block = *m_firstBlock;
        Block* nextBlockPtr = current_block.next;
        int count = 0;
        while (nextBlockPtr != nullptr)
        {
            if (count < *lastBlockUpdate)
            {
                continue;
            }

            (*wallets)[current_block.miner] += MONEY_CREATED_FOR_THE_MINER_EACH_BLOCK;
            Satoshi blockFees = 0;
            for (auto tx : current_block.trans)
            {
                Address from = tx.from;
                Address to = tx.to;
                std::map<Address, Satoshi>::iterator fromIt = wallets->find(from);
                std::map<Address, Satoshi>::iterator toIt = wallets->find(to);

                if (fromIt == wallets->end())
                {
                    wallets->insert({ from, 0 });
                }
                if (toIt == wallets->end())
                {
                    wallets->insert({ to, 0 });
                }
                wallets->at(from) -= (tx.amount + tx.fee);
                wallets->at(to) + -tx.amount;
                blockFees += tx.fee;
            }
            
            wallets->at(current_block.miner) += blockFees;
            
            current_block = *nextBlockPtr;
            nextBlockPtr = current_block.next;
            count++;
        }
        *lastBlockUpdate = count;
    }

    bool TxExists(const Transaction &tx)const
    {
        Block current_block = *m_firstBlock;
        Block* nextBlockPtr = current_block.next;

        while (nextBlockPtr != nullptr)
        {
            if (current_block.TxExists(tx))
            {
                return true;
            }                
            
            current_block = *nextBlockPtr;
            nextBlockPtr = current_block.next;
        }

        return false;
    }
};


template <class T, std::size_t N>
ostream& operator<<(ostream& o, const array<T, N>& arr)
{
    copy(arr.cbegin(), arr.cend(), ostream_iterator<T>(o, " "));
    return o;
}

int main()
{
    std::array<int, 4> a{ {1, 2, 3, 4} };
    auto result = general::f(a);
    std::cout <<"Output of 1, 2, 3 is: " << general::Sum(1, 2, 3) << std::endl;
    std::cout << "Output of array {1, 2, 3, 4} is " << result << std::endl;
    std::cout << "Hello Blockchain! Very nice to meet you! My name is main()" << std::endl;
    return 0;
}