#ifndef __FUNCTIONS_H__
#define __FUNCTIONS_H__

#include <iostream>
#include <vector>
#include <limits>
#include <array>

namespace general
{
	template <typename ...T>
	int Sum(T... args);


	template<size_t T>
	std::array<int, T / 2> f(std::array<int, T> arr);
}


#include "functions_inl.h"
#endif //__FUNCTIONS_H__
