#ifndef __FUNCTIONS_INL_H__
#define __FUNCTIONS_INL_H__

#include "functions.h"

template <typename ...T>
int general::Sum(T... args)
{
    int sum = 0;
    auto a = { std::forward<T>(args)... };
    for (const auto i : a)
        sum += i;

    return sum;
}

template<size_t T>
std::array<int, T / 2> general::f(std::array<int, T> arr)
{
    std::array<int, T / 2> newArr = std::array<int, T/2>();
    size_t arrSize = arr.size();
    for (size_t i = 0; i < arrSize / 2; i++)
    {
        size_t idx = 2 * i;
        newArr.at(i) = arr.at(idx) + arr.at(idx + 1);
    }
    return newArr;
}

#endif //__FUNCTIONS_INL_H__